<?php

namespace DreamCat\FrameDemo\Popo\Demo\UserQuery;

use DreamCat\FrameDemo\Entity\Mysql\UserEntity;

/**
 * 查询用户列表的输出VO
 * @author vijay
 */
class UserQueryOutVo
{
    /** @var UserEntity[] 查到的用户列表 */
    private $list;

    /**
     * @return UserEntity[] 查到的用户列表
     */
    public function getList(): array
    {
        return $this->list;
    }

    /**
     * @param UserEntity[] $list 查到的用户列表
     * @return UserQueryOutVo
     */
    public function setList(array $list): UserQueryOutVo
    {
        $this->list = $list;
        return $this;
    }
}

# end of file
