<?php

/**
 * Web 入口页面
 * @author wangj
 */

require_once __DIR__ . "/vendor/autoload.php";

\DreamCat\FrameCore\FpmEntrance::start();

# end of file
