# cat_framework_demo

## 介绍
喵框架初始化代码，同时也是示例代码

## 安装教程
1. 下载项目
    ```
    composer create-project dreamcat/cat_frame_demo
    ```
1. 修改 `composer.json` 中 `name` `description` `authors` `autoload` `autoload-dev`，注意，如果去除原有自动引入，现在的示例代码将无法执行，可以删除。
1. 参考示例修改web容器配置。示例中php项目代码路径是 `/home/php`，相应前端路径 `/home/static`
    - Apache
    ```apacheconfig
    <VirtualHost *:80>
        DocumentRoot "/home/php"
        ServerName www.demo.com
        
        <Directory "/home/php">
            Require all granted
            AllowOverride All
            Allow from all
        </Directory>
        
        <IfModule mod_rewrite.c>
            RewriteEngine on
            RewriteRule ^(.*) /web.php [QSA,PT,L]
        </IfModule>
    </VirtualHost>
    ```
    - nginx
    ```nginxconfig
    server {
        listen       80;
        server_name  www.demo.com;
        index        index.html;
        root         /dev/null;
       
        # 首页指向前端，根据自己的业务需求调整
        location = / {
            rewrite ^/$ /static/ permanent; 
        }
        
        location /static/ {
          alias /home/static/dist/; 
          index index.html;
              
          location ~* \.(css|js|gif|jpe?g|png)$ {
              expires 1M;
              add_header Pragma public;
              add_header Cache-Control "public, must-revalidate, proxy-revalidate";
          }
        }
        
        # 接口服务器
        location /api {
          try_files $uri $uri/ /web.php?$query_string; 
        }
    
        location ~* \.php$ {
            fastcgi_pass     unix:/php/var/run/php_web.socket;
            fastcgi_index index.php;
            include fastcgi.conf;
            fastcgi_split_path_info ^(.+.php)(/.+)$;
            fastcgi_param SCRIPT_FILENAME /home/php/web.php;
            fastcgi_param HTTP_PROXY "";
        }
    
    }
    ```
1. 配置数据库、日志、路由，各环境一致的配置在 [`configs/config.php`](configs/config.php) 中，随环境变化的配置放 `cache/configs/config.php` 中
1. 编写控制器逻辑

## 使用说明
可以参考示例的控制器和model，或者参考 [wiki](https://gitee.com/vijay_0215/cat_framework_demo/wikis/pages)

建议参考示例中的分层，这样后续基建的工作就可以为之服务。不过如果是很简单的逻辑，也可以用简易方式即控制器完成一切的模式。
- `Controller`下放控制器
- `Entry` 下放 model 与数据库交互的实体对象
- `Model` 下放 model
- `Pojo` 下放控制器与外部交互的数据结构
- `Service` 下放实现业务逻辑的 Service。

